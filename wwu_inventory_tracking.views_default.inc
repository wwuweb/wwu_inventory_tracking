<?php
/**
 * @file
 * wwu_inventory_tracking.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function wwu_inventory_tracking_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'event_attendees';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'entityform';
  $view->human_name = 'Event Attendees';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'access all webform results';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'entityform_id' => 'entityform_id',
    'field_event' => 'field_event',
    'field_your_name' => 'field_your_name',
    'field_your_email' => 'field_your_email',
    'field_number_of_seats' => 'field_number_of_seats',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'entityform_id' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_event' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_your_name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_your_email' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_number_of_seats' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Entityform Submission: Entityform submission ID */
  $handler->display->display_options['fields']['entityform_id']['id'] = 'entityform_id';
  $handler->display->display_options['fields']['entityform_id']['table'] = 'entityform';
  $handler->display->display_options['fields']['entityform_id']['field'] = 'entityform_id';
  $handler->display->display_options['fields']['entityform_id']['label'] = '';
  $handler->display->display_options['fields']['entityform_id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['entityform_id']['element_label_colon'] = FALSE;
  /* Field: Entityform Submission: Event */
  $handler->display->display_options['fields']['field_event']['id'] = 'field_event';
  $handler->display->display_options['fields']['field_event']['table'] = 'field_data_field_event';
  $handler->display->display_options['fields']['field_event']['field'] = 'field_event';
  $handler->display->display_options['fields']['field_event']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_event']['settings'] = array(
    'link' => 0,
  );
  $handler->display->display_options['fields']['field_event']['field_api_classes'] = TRUE;
  /* Field: Entityform Submission: Your Name */
  $handler->display->display_options['fields']['field_your_name']['id'] = 'field_your_name';
  $handler->display->display_options['fields']['field_your_name']['table'] = 'field_data_field_your_name';
  $handler->display->display_options['fields']['field_your_name']['field'] = 'field_your_name';
  $handler->display->display_options['fields']['field_your_name']['label'] = 'Name';
  $handler->display->display_options['fields']['field_your_name']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_your_name']['field_api_classes'] = TRUE;
  /* Field: Entityform Submission: Your Email Address */
  $handler->display->display_options['fields']['field_your_email']['id'] = 'field_your_email';
  $handler->display->display_options['fields']['field_your_email']['table'] = 'field_data_field_your_email';
  $handler->display->display_options['fields']['field_your_email']['field'] = 'field_your_email';
  $handler->display->display_options['fields']['field_your_email']['label'] = 'Email';
  $handler->display->display_options['fields']['field_your_email']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_your_email']['field_api_classes'] = TRUE;
  /* Field: Entityform Submission: Number of Seats */
  $handler->display->display_options['fields']['field_number_of_seats']['id'] = 'field_number_of_seats';
  $handler->display->display_options['fields']['field_number_of_seats']['table'] = 'field_data_field_number_of_seats';
  $handler->display->display_options['fields']['field_number_of_seats']['field'] = 'field_number_of_seats';
  $handler->display->display_options['fields']['field_number_of_seats']['label'] = 'Seats';
  $handler->display->display_options['fields']['field_number_of_seats']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_number_of_seats']['settings'] = array(
    'thousand_separator' => ' ',
    'prefix_suffix' => 1,
  );
  $handler->display->display_options['fields']['field_number_of_seats']['field_api_classes'] = TRUE;
  /* Filter criterion: Entityform Submission: Entityform Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'entityform';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'rsvp_form' => 'rsvp_form',
  );
  /* Filter criterion: Entityform Submission: Event (field_event) */
  $handler->display->display_options['filters']['field_event_target_id']['id'] = 'field_event_target_id';
  $handler->display->display_options['filters']['field_event_target_id']['table'] = 'field_data_field_event';
  $handler->display->display_options['filters']['field_event_target_id']['field'] = 'field_event_target_id';
  $handler->display->display_options['filters']['field_event_target_id']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_event_target_id']['expose']['operator_id'] = 'field_event_target_id_op';
  $handler->display->display_options['filters']['field_event_target_id']['expose']['label'] = 'Event';
  $handler->display->display_options['filters']['field_event_target_id']['expose']['operator'] = 'field_event_target_id_op';
  $handler->display->display_options['filters']['field_event_target_id']['expose']['identifier'] = 'field_event_target_id';
  $handler->display->display_options['filters']['field_event_target_id']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    6 => 0,
    5 => 0,
    4 => 0,
    3 => 0,
  );

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $handler->display->display_options['inherit_panels_path'] = '1';
  $export['event_attendees'] = $view;

  $view = new view();
  $view->name = 'rsvp_able_events';
  $view->description = 'Populates the options for the entity reference field on the RSVP form';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'RSVP-able Events';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'event' => 'event',
  );
  /* Filter criterion: Content: Remaining Seats (field_remaining_seats) */
  $handler->display->display_options['filters']['field_remaining_seats_value']['id'] = 'field_remaining_seats_value';
  $handler->display->display_options['filters']['field_remaining_seats_value']['table'] = 'field_data_field_remaining_seats';
  $handler->display->display_options['filters']['field_remaining_seats_value']['field'] = 'field_remaining_seats_value';
  $handler->display->display_options['filters']['field_remaining_seats_value']['operator'] = 'not empty';
  /* Filter criterion: Content: Remaining Seats (field_remaining_seats) */
  $handler->display->display_options['filters']['field_remaining_seats_value_1']['id'] = 'field_remaining_seats_value_1';
  $handler->display->display_options['filters']['field_remaining_seats_value_1']['table'] = 'field_data_field_remaining_seats';
  $handler->display->display_options['filters']['field_remaining_seats_value_1']['field'] = 'field_remaining_seats_value';
  $handler->display->display_options['filters']['field_remaining_seats_value_1']['operator'] = '>';
  $handler->display->display_options['filters']['field_remaining_seats_value_1']['value']['value'] = '0';
  /* Filter criterion: Content: Date -  start date (field_start_date) */
  $handler->display->display_options['filters']['field_start_date_value']['id'] = 'field_start_date_value';
  $handler->display->display_options['filters']['field_start_date_value']['table'] = 'field_data_field_start_date';
  $handler->display->display_options['filters']['field_start_date_value']['field'] = 'field_start_date_value';
  $handler->display->display_options['filters']['field_start_date_value']['operator'] = '>=';
  $handler->display->display_options['filters']['field_start_date_value']['default_date'] = 'now';
  $handler->display->display_options['filters']['field_start_date_value']['year_range'] = '-3:+0';

  /* Display: Entity Reference */
  $handler = $view->new_display('entityreference', 'Entity Reference', 'entityreference_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '95';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'entityreference_style';
  $handler->display->display_options['style_options']['search_fields'] = array(
    'title' => 'title',
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'entityreference_fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $export['rsvp_able_events'] = $view;

  return $export;
}
