A Drupal feature used for migrating the seat tracking/RSVP system to CFPA's production site.

This feature is not generalized, and is only intended for use on WWU's CFPA site.

Before importing and enabling this feature, make sure the "Event" content type has an integer field with machine name "field\_remaining_seats"


_USING THIS FEATURE_

To enable seat tracking for an event, put a value in the "Remaining Seats" field. When a user submits the RSVP form with this event chosen, this field wil automatically decrement appropriately.

By default, when someone visits the RSVP form page, they will be able to choose which event they want to RSVP for from a drop down list. To disable this field and link to the RSVP from for a specific event only, append ?field_even=nid to the URL when you link to the form, where nid is the node id of the event. There is documentation on the WebTech site for finding a node's id.

There is a view included called "Event Attendees" which displays a list of form submissions sortable by event.


_POTENTIAL ISSUES_

- No error handling when a user submits the form incorrectly (i.e. asks for 5 seats when there are only 4 left). In this case, the seats will not be decremented and a confirmation email will not be sent, but the submission will still show in reports, and there is no error message displayed to the user. (Note that if there are no seats left, the event will no longer be RSVP-able).

- If an event runs out of seats, it is removed from the select list of events on the RSVP form so users can no longer RSVP for it. However, links that previously specified that event in the URL (via ?field_event=nid) continue to link to the form page with the dropdown now enabled. These links should be removed manually.

- When an event's date has passed (i.e. it is over) it is removed from the list of RSVP-able events. However, since this list populates the filter for the Event Attendees view, you can no longer filter to see submissions only for that event.