<?php
/**
 * @file
 * wwu_inventory_tracking.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function wwu_inventory_tracking_default_rules_configuration() {
  $items = array();
  $items['rules_decrement_seats_after_rsvp'] = entity_import('rules_config', '{ "rules_decrement_seats_after_rsvp" : {
      "LABEL" : "Decrement Seats After RSVP",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "entityform" ],
      "ON" : { "entityform_insert" : [] },
      "IF" : [
        { "entity_is_of_bundle" : {
            "entity" : [ "entityform" ],
            "type" : "entityform",
            "bundle" : { "value" : { "rsvp_form" : "rsvp_form" } }
          }
        },
        { "node_is_of_type" : {
            "node" : [ "entityform:field-event" ],
            "type" : { "value" : { "event" : "event" } }
          }
        },
        { "NOT data_is_empty" : { "data" : [ "entityform:field-event:field-remaining-seats" ] } },
        { "NOT data_is" : {
            "data" : [ "entityform:field-number-of-seats" ],
            "op" : "\\u003E",
            "value" : [ "entityform:field-event:field-remaining-seats" ]
          }
        }
      ],
      "DO" : [
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "entityform:field-event:field-remaining-seats" ],
              "op" : "-",
              "input_2" : [ "entityform:field-number-of-seats" ]
            },
            "PROVIDE" : { "result" : { "new_number_of_seats" : "New Number of Seats" } }
          }
        },
        { "data_set" : {
            "data" : [ "entityform:field-event:field-remaining-seats" ],
            "value" : [ "new_number_of_seats" ]
          }
        },
        { "mail" : {
            "to" : [ "entityform:field-your-email" ],
            "subject" : "Your Event Reservation",
            "message" : "[entityform:field-your-name],\\r\\n\\r\\nThank you for RSVP-ing for [entityform:field-which-event:title]. You have reserved [entityform:field_number_of_seats] seats. We look forward to seeing you there!\\r\\n\\r\\nWWU College of Fine and Performing Arts",
            "from" : [ "site:mail" ],
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  return $items;
}
