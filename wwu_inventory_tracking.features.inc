<?php
/**
 * @file
 * wwu_inventory_tracking.features.inc
 */

/**
 * Implements hook_views_api().
 */
function wwu_inventory_tracking_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_entityform_type().
 */
function wwu_inventory_tracking_default_entityform_type() {
  $items = array();
  $items['rsvp_form'] = entity_import('entityform_type', '{
    "type" : "rsvp_form",
    "label" : "RSVP Form",
    "data" : {
      "draftable" : 0,
      "draft_redirect_path" : "",
      "draft_button_text" : "",
      "draft_save_text" : { "value" : "", "format" : "clean_html" },
      "submit_button_text" : "RSVP",
      "submit_confirm_msg" : "",
      "your_submissions" : "",
      "disallow_resubmit_msg" : "",
      "delete_confirm_msg" : "",
      "page_title_view" : "",
      "preview_page" : 0,
      "submission_page_title" : "",
      "submission_text" : {
        "value" : "\\u003Cp\\u003EA confirmation has been sent to the email address you provided. See you soon!\\u003C\\/p\\u003E\\r\\n",
        "format" : "clean_html"
      },
      "submission_show_submitted" : 1,
      "submissions_view" : "default",
      "user_submissions_view" : "default",
      "form_status" : "ENTITYFORM_OPEN",
      "roles" : { "1" : "1", "2" : "2", "6" : "6", "5" : "5", "4" : "4", "3" : "3" },
      "resubmit_action" : "new",
      "redirect_path" : "",
      "instruction_pre" : { "value" : "", "format" : "clean_html" }
    },
    "weight" : "0",
    "rdf_mapping" : [],
    "paths" : {
      "submit" : {
        "source" : "eform\\/submit\\/rsvp-form",
        "alias" : "rsvp",
        "language" : "und"
      },
      "confirm" : {
        "source" : "eform\\/rsvp-form\\/confirm",
        "alias" : "rsvp\\/confirm",
        "language" : "und"
      }
    }
  }');
  return $items;
}
